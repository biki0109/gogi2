package client

import (
	"context"

	"git.begroup.team/platform-core/kitchen/l"
	"git.begroup.team/platform-transport/gogi2/pb"
	"google.golang.org/grpc"
	"google.golang.org/grpc/balancer/roundrobin"
)

type Gogi2Client struct {
	pb.Gogi2Client
}

func NewGogi2Client(address string) *Gogi2Client {
	conn, err := grpc.DialContext(context.Background(), address,
		grpc.WithInsecure(),
		grpc.WithBalancerName(roundrobin.Name),
	)

	if err != nil {
		ll.Fatal("Failed to dial Gogi2 service", l.Error(err))
	}

	c := pb.NewGogi2Client(conn)

	return &Gogi2Client{c}
}
