// IM AUTO GENERATED, BUT CAN BE OVERRIDDEN

package main

import (
	"git.begroup.team/platform-transport/gogi2/config"
	"git.begroup.team/platform-transport/gogi2/internal/services"
	"git.begroup.team/platform-transport/gogi2/internal/stores"
	"git.begroup.team/platform-transport/gogi2/pb"
)

func registerService(cfg *config.Config) pb.Gogi2Server {

	mainStore := stores.NewMainStore()

	return services.New(cfg, mainStore)
}
